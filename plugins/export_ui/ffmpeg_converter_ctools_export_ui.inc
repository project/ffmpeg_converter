<?php

/**
 * @file
 * A Ctools Export UI plugin for FFmpeg Converter presets.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'ffmpeg_converter_preset',
  'access' => 'administer ffmpeg wrapper',
  'menu' => array(
    'menu item' => 'ffmpeg_converter',
    'menu prefix' => 'admin/settings',
    'menu title' => 'FFmpeg Converter',
    'menu description' => 'Administer FFmpeg Converter presets.',
  ),

  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('FFmpeg Converter preset'),
  'title plural proper' => t('FFmpeg Converter presets'),

  'handler' => array(
    'class' => 'ffmpeg_converter_presets_ui',
    'parent' => 'ctools_export_ui',
  ),

  'form' => array(
    'settings' => 'ffmpeg_converter_ctools_export_ui_form',
    'validate' => 'ffmpeg_converter_ctools_export_ui_form_validate',
    'submit' => 'ffmpeg_converter_ctools_export_ui_form_submit',
  ),
);

/**
 * Define the preset add/edit form.
 */
function ffmpeg_converter_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The human readable name or description of this preset.'),
    '#default_value' => $preset->description,
    '#required' => true,
  );

  // Add FFmpeg Wrapper's configuration form.
  if (function_exists('ffmpeg_wrapper_ui_configuration_form')) {
    $form += ffmpeg_wrapper_ui_configuration_form($preset->ffmpeg_wrapper, "edit-ffmpeg-converter-presets-$preset->name-ffmpeg-wrapper-");
    $form['ffmpeg_wrapper']['#collapsible'] = true;
    $form['ffmpeg_wrapper']['#tree'] = true;
  }
  else {
    drupal_set_message(t('Note: FFmpeg Wrapper UI needs to be enabled in order for this configuration page to work. Please enable it on the <a href="!url">modules page</a>.', array('!url' => url('admin/build/modules'))), 'warning');
  }
}

/**
 * Validation handler for the preset edit form.
 */
function ffmpeg_converter_ctools_export_ui_form_validate($form, &$form_state) {
  if (!isset($form_state['values']['ffmpeg_wrapper'])) {
    form_set_error('ffmpeg_wrapper', t('The FFmpeg Wrapper configuration is missing.'));
  }
}

/**
 * Submit handler for the preset edit form.
 */
function ffmpeg_converter_ctools_export_ui_form_submit($form, &$form_state) {
  // Flatten and serialize the ffmpeg_wrapper settings array.
  $form_state['values']['ffmpeg_wrapper'] = serialize(_ffmpeg_converter_array_flatten($form_state['values']['ffmpeg_wrapper']));
}
