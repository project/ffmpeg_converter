<?php

/**
 * @file
 * A custom Ctools Export UI class for FFmpeg Converter presets.
 */

/**
 * Customizations of the FFmpeg Converter presets UI.
 */
class ffmpeg_converter_presets_ui extends ctools_export_ui {

  /**
   * Alter the preset list form defined by the base class.
   */
  function list_form(&$form, &$form_state) {
    parent::list_form($form, $form_state);

    $help = t('Use this page to create and modify FFmpeg configuration presets which you can use to process media files automatically.');
    if (module_exists('filefield') && module_exists('drupal_queue') && module_exists('rules')) {
      $help .= ' ' . t('Your presets will appear in <a href="@rules">Rules</a> using the "Queue file field for conversion" action, which lets you create rules that convert all media files that are uploaded to a certain file field automatically.', array('@rules' => url('admin/rules/trigger')));
    }
    else {
      $help .= ' ' . t('If you install and enable the Rules, Drupal Queue and FileField modules, your presets will appear on rule configuration pages, which allows you to have all media files that are uploaded to a certain file field processed automatically.');
    }
    $help .= ' ' . t('Other modules may also make this conversion functionality available in their own specific contexts.');
    $help = '<p class="ffmpeg-converter-help">' . $help . '</p>';

    $form['#prefix'] .= $help;
  }
}
