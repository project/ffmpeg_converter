
-------------------------------
FFMPEG CONVERTER MIGRATE README
-------------------------------

This tool is used to convert existing content from the old single-field storage
scheme used in FFmpeg Converter 6.x-1.x, to the new recommended scheme, where
each file type is stored in a separate field. Use this tool if you are switching
to the Rules based conversion in the 6.x-2.x version, or if you just want to
migrate to a different workflow.


INSTRUCTIONS
------------

Activate this module and navigate to:
  Administer > Content > Migrate FFmpeg Converter files

The migration page contains further instructions. When the migration is
complete, you should disable this module.
