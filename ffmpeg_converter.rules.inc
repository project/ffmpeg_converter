<?php


/**
 * @file
 * Rules integration for FFmpeg Converter.
 */

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function ffmpeg_converter_rules_action_info() {
  return array(
    'ffmpeg_converter_action_queue_filefield' => array(
      'label' => t('Queue file field for conversion'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
      ),
      'module' => 'ffmpeg_converter',
    ),
  );
}

/**
 * Configuration form for the queue filefield action.
 */
function ffmpeg_converter_action_queue_filefield_form($settings, &$form) {
  $settings += array(
    'source' => '',
    'destination' => '',
    'preset' => '',
  );

  // Load CCK fields and make a list of available file fields.
  $fields = content_fields();
  $file_field_options = array();
  foreach ($fields as $field) {
    if ($field['type'] == 'filefield') {
      $file_field_options[$field['field_name']] = $field['widget']['label'];
    }
  }

  $form['settings']['source'] = array(
    '#type' => 'select',
    '#title' => t('Source field'),
    '#options' => $file_field_options,
    '#default_value' => $settings['source'],
  );

  $form['settings']['destination'] = array(
    '#type' => 'select',
    '#title' => t('Destination field'),
    '#options' => $file_field_options,
    '#default_value' => $settings['destination'],
  );

  $form['settings']['preset'] = array(
    '#type' => 'select',
    '#title' => t('Preset'),
    '#options' => ffmpeg_converter_presets(),
    '#default_value' => $settings['preset'],
  );

}

/**
 * Validate the configuration form.
 */
function ffmpeg_converter_action_queue_filefield_validate($form, $form_state) {
  if ($form_state['values']['settings']['source'] == $form_state['values']['settings']['destination']) {
    form_set_error('settings][destination', t('The source and destination cannot be the same field.'));
  }
}

/**
 * Help text for the configuration form.
 */
function ffmpeg_converter_action_queue_filefield_help() {
  return t('Please specify which field contains the source file(s) and which field should recieve the converted file(s). Both of these fields need to exist in the same node type. Also choose the <a href="@url">FFmpeg Converter preset</a> you want to use for this conversion.', array('@url' => url('admin/settings/ffmpeg_converter')));
}

/**
 * Run the queue filefield action.
 */
function ffmpeg_converter_action_queue_filefield(&$node, $settings) {
  $source_field = $settings['source'];
  $dest_field = $settings['destination'];
  $preset_key = $settings['preset'];

  // Look for the ignore flag set by running conversion jobs.
  if (empty($node->_ffmpeg_converter_ignore)) {

    // Check all the necessary prerequisites.
    $source_field_def = content_fields($source_field, $node->type);
    $dest_field_def = content_fields($dest_field, $node->type);
    $presets = ffmpeg_converter_presets();

    if (!$source_field_def) {
      drupal_set_message(t('This content could not be converted with FFmpeg Converter since the source field doesn\'t exist in this content type.'), 'error');
    }
    elseif (!$dest_field_def) {
      drupal_set_message(t('This content could not be converted with FFmpeg Converter since the destination field doesn\'t exist in this content type.'), 'error');
    }
    elseif ($source_field == $dest_field) {
      drupal_set_message(t('This content could not be converted with FFmpeg Converter since the source and destination fields are the same.'), 'error');
    }
    elseif (!isset($presets[$preset_key])) {
      drupal_set_message(t('This content could not be converted with FFmpeg Converter since the conversion preset doesn\'t exist.'), 'error');
    }
    else {
      // Queue the source field's files for conversion.
      foreach ($node->{$source_field} as $field_instance => $file) {
        // Look for an existing destination file.
        if (!empty($node->{$dest_field}[$field_instance]['filepath'])) {
          drupal_set_message(t('The file %file will not be converted since there is already a file in the corresponding slot in the destination field. If you wish to replace it, please delete it first.', array('%file' => $file['filename'])));
        }
        elseif (is_array($file) && !empty($file['filepath']) && file_exists($file['filepath'])) {
          if (!ffmpeg_converter_file_is_queued($node->nid, $source_field, $dest_field, $field_instance)) {
            ffmpeg_converter_queue_filefield($node->nid, $source_field, $dest_field, $field_instance, $preset_key);
            drupal_set_message(t('The file %file was queued for conversion.', array('%file' => $file['filename'])));
          }
          else {
            drupal_set_message(t('The file %file is already in the job queue.', array('%file' => $file['filename'])));
          }
        }
      }
    }
  }
}
