
-----------------------
FFMPEG CONVERTER README
-----------------------

This module is designed to provide a user interface for the FFmpeg Wrapper 
module (http://drupal.org/project/ffmpeg_wrapper). It automatically converts 
media files to different destination formats and allows you to change a number 
of properties such as pixel size, bitrate etc.


Installation
------------

1. Make sure you have a working installation of FFmpeg on your web server. The 
   version that comes with your OS distribution (if any) may not be sufficient.
   Share hosts usually don't have FFmpeg installed.
   
   - FFmpeg home: http://ffmpeg.mplayerhq.hu/
   - Ubuntu instructions:
       Using Medibuntu - http://ubuntuforums.org/showthread.php?t=911849
       Compiling - http://ubuntuforums.org/showthread.php?t=786095
       Compiling - https://wiki.ubuntu.com/ffmpeg
   - See also FFmpeg Wrapper's README file.

2. Download and install FFmpeg Wrapper which is required by FFmpeg Converter:

   - http://drupal.org/project/ffmpeg_wrapper
   
3. Download and install these recommended modules (not much will happen without
   them):
   
   - FileField: http://drupal.org/project/filefield
   - Drupal Queue: http://drupal.org/project/drupal_queue
   - Rules: http://drupal.org/project/rules
   
4. Optionally download and install MimeDetect, which will set proper MIME types 
   on your converted files.

   - http://drupal.org/project/mimedetect
   
5. Install and activate this module.


Configuration
-------------

1. Go to Administer > Site Configuration > FFmpeg Wrapper and supply the proper
   file paths. See the README file in the FFmpeg Wrapper directory for more
   information.
   
2. Go to Administer > Site Building > FFmpeg Converter and set up your 
   conversion preset(s). You may use the predefined presets, override them, or 
   create your own from scratch. For more information about the FFmpeg options
   go to: http://ffmpeg.mplayerhq.hu/

3. Go to Administer > Content > Content types and set up (at least) two file
   fields for one of your node types. One will contain the original files and
   the other will be used for converted files. Make sure you allow the file 
   extensions you plan to use, and sufficient file sizes. It is recommended that
   you allow single values only for these fields, although multiple values do 
   work, in theory at least. (The potential problem is that the order of the 
   files in the different fields may get mixed up when files are added or 
   deleted.)

4. Go to Administer > Rules > Triggered rules. Add a new rule which is triggered
   on "After saving new content". Add some suitable conditions to check things
   like the node type being correct and the source field being populated. Then
   add the action "Queue file field for conversion", using file fields and a
   preset that you defined in the previous steps.
   Add another rule for "After updating existing content" if you like.

   All media files uploaded to the source field should now be automatically
   converted if you have Drupal Queue installed and cron set up (see
   http://drupal.org/cron and Drupal Queue's README.txt file).
   
5. Test you configuration. You will probably want to allow PHP to execute for a
   longer time than the usual default. This can be configured in your php.ini 
   file. You may also want to test different cron intervals.


Use
---

When creating or editing a node, add an input file to your configured source
field. After saving, you should see a message saying that the file has been 
queued for conversion. After the next cron run, or after a couple, the 
destination file field should contain a converted copy of the uploaded file.

If you want to redo the conversion at a later time, edit the node and delete the
file(s) in the destination field. When you save, the conversion should be queued
again.

FFmpeg converter doesn't provide any sort of display or output functionality, 
such as video players etc. You have to rely on other modules or your own theming
to achieve this.
