<?php


/**
 * @file
 * Include file for FFmpeg Converter's administration pages.
 */

/**
 * Page callback: configuration form.
 */
function ffmpeg_converter_admin_configuration() {
  $form = array();

  $form['ffmpeg_converter_timeout'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Conversion time limit'),
    '#field_suffix' => ' ' . t('sec'),
    '#description' => t('Specify the number of seconds FFmpeg Converter will be allowed to spend converting files each time the convertion queue runs. Note that this is not a hard timeout, but rather a time limit for initiating new conversions.'),
    '#default_value' => variable_get('ffmpeg_converter_timeout', 30),
  );
  $form['ffmpeg_converter_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save debug messages to the log'),
    '#description' => t('If you enable this, the options used when invoking ffmpeg will be logged each time a file is converted.'),
    '#default_value' => variable_get('ffmpeg_converter_debug', 0),
  );

  return system_settings_form($form);
}
